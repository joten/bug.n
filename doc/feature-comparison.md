# A Feature Comparison of bug.n redux with bug.n (v9)

## Configuration Variables in bug.n's `Config.ini`

### Status Bar
| Name               | Value                                                             | bug.n Comment                                             | bug.n | bug.n redux | bug.n redux Comment      |
|:-------------------|:------------------------------------------------------------------|:----------------------------------------------------------|:-----:|:-----------:|:-------------------------|
| showBar            | True                                                              |                                                           |   ✅   |      ✅      |                          |
| horizontalBarPos   | "left"                                                            |                                                           |   ✅   |      ✅      |                          |
| verticalBarPos     | "top"                                                             |                                                           |   ✅   |      ❌      | `"tray"` only            |
| barWidth           | "100%"                                                            |                                                           |   ✅   |      ✅      |                          |
| singleRowBar       | True                                                              |                                                           |   ✅   |      ❌      | `True` only              |
| spaciousBar        | False                                                             |                                                           |   ✅   |      ❌      | more or less `True` only |
| fontName           | "Lucida Console"                                                  |                                                           |   ✅   |      ✅      |                          |
| largeFontSize      | 24                                                                | for trace areas                                           |   ✅   |      ❌      |                          |
| fontSize           |                                                                   | By default `fontSize` and `*color`                        |   ✅   |      ✅      |                          |
| backColor_#{1,2,3} |                                                                   | values are read from system settings                      |   ✅   |      ✅      |                          |
| foreColor_#{1,2,3} |                                                                   | `foreColor` allowing frames and progress bars             |   ✅   |      ✅      |                          |
| fontColor_#{1,2,3} |                                                                   | All colors are set individually for each element read in. |   ✅   |      ✅      |                          |
| barTransparency    | "off"                                                             |                                                           |   ✅   |      ✅      |                          |
| barCommands        | "Run, explore " Main_docDir ";Monitor_toggleBar();Reload;ExitApp" |                                                           |   ✅   |      ❌      |                          |
| readinBat          | False                                                             |                                                           |   ✅   |      ❌      |                          |
| readinCpu          | False                                                             |                                                           |   ✅   |      ❌      |                          |
| readinDate         | True                                                              | with `readinDateFormat := "ddd, dd. MMM. yyyy"`           |   ✅   |      ❌      |                          |
| readinDiskLoad     | False                                                             |                                                           |   ✅   |      ❌      |                          |
| readinMemoryUsage  | False                                                             |                                                           |   ✅   |      ❌      |                          |
| readinNetworkLoad  | False                                                             |                                                           |   ✅   |      ❌      |                          |
| readinTime         | True                                                              | with `readinTimeFormat := "HH:mm"`                        |   ✅   |      ❌      |                          |
| readinVolume       | False                                                             |                                                           |   ✅   |      ❌      |                          |
| readinAny()        |                                                                   | Text returnd by a function.                               |   ✅   |      ❌      |                          |
| readinInterval     | 30000                                                             |                                                           |   ✅   |      ❌      |                          |

### Windows UI Elements
| Name            | Value | bug.n Comment       | bug.n | bug.n redux | bug.n redux Comment     |
|:----------------|:------|:--------------------|:-----:|:-----------:|:------------------------|
| bbCompatibility | False |                     |   ✅   |      ❌      |                         |
| borderWidth     | 0     | not with Windows 10 |   ✅   |      ❌      |                         |
| borderPadding   | -1    | not with Windows 10 |   ✅   |      ❌      |                         |
| showTaskBar     | False | not with Windows 10 |   ✅   |      ❌      |                         |
| showBorder      | True  | not with Windows 10 |   ✅   |      ❌      |                         |
| selBorderColor  | ""    |                     |   ✅   |      ❌      |                         |
| scalingFactor   | 1     |                     |   ✅   |      ✅      | automatically retrieved |

### Window Arrangement
| Name                   | Value                                                      | bug.n Comment                       | bug.n | bug.n redux | bug.n redux Comment                             |
|:-----------------------|:-----------------------------------------------------------|:------------------------------------|:-----:|:-----------:|:------------------------------------------------|
| viewNames              | "1;2;3;4;5;6;7;8;9" (any text)                             |                                     |   ✅   |      ✅      |                                                 |
| layout_#1              | "[]=;tile"                                                 |                                     |   ✅   |      ✅      |                                                 |
| layout_#2              | "[M];monocle"                                              |                                     |   ✅   |      ✅      |                                                 |
| layout_#3              | "><>;"                                                     |                                     |   ✅   |      ✅      |                                                 |
| layoutAxis_#1          | 1                                                          |                                     |   ✅   |      ❌      | There is not one complex layout,                |
| layoutAxis_#2          | 2                                                          |                                     |   ✅   |      ❌      | but array with a succession of                  |
| layoutAxis_#3          | 2                                                          |                                     |   ✅   |      ❌      | distinct layouts.                               |
| layoutGapWidth         | 0                                                          |                                     |   ✅   |      ❌      |                                                 |
| layoutMFactor          | 0.6                                                        |                                     |   ✅   |      ✅      |                                                 |
| areaTraceTimeout       | 1000                                                       | or `continuouslyTraceAreas := True` |   ✅   |      ❌      |                                                 |
| continuouslyTraceAreas | False                                                      |                                     |   ✅   |      ❌      |                                                 |
| dynamicTiling          | True                                                       |                                     |   ✅   |      ❌      | `True` only, no manual tiling                   |
| ghostWndSubString      | " (Not Responding)"                                        |                                     |   ✅   |      ❌      | Only working for WIndows 7?                     |
| mFactCallInterval      | 700                                                        |                                     |   ✅   |      ❌      |                                                 |
| mouseFollowsFocus      | True                                                       |                                     |   ✅   |      ❌      |                                                 |
| newWndPosition         | "top"                                                      |                                     |   ✅   |      ❌      | `"bottom"` only                                 |
| onActiveHiddenWnds     | "view"                                                     |                                     |   ✅   |      ❌      | `"view"` only                                   |
| shellMsgDelay          | 350                                                        |                                     |   ✅   |      ❌      |                                                 |
| syncMonitorViews       | 0                                                          |                                     |   ✅   |      ❌      |                                                 |
| viewFollowsTagged      | False                                                      |                                     |   ✅   |      ❌      |                                                 |
| viewMargins            | "0;0;0;0"                                                  |                                     |   ✅   |      ❌      |                                                 |
| rule_#*                | `"<class>;<title>;<function name>;<is managed>;<m>;<tags>` |                                     |   ✅   |      ✅      | not `<m>`,                                      |
|                        | `;<is floating>;<is decorated>;<hide title>;<action>"`     |                                     |       |      ❌      | `<is decorated>`, `<hide title>` and `<action>` |

### Configuration Management
| Name                         | Value  | bug.n Comment                               | bug.n | bug.n redux | bug.n redux Comment           |
|:-----------------------------|:-------|:--------------------------------------------|:-----:|:-----------:|:------------------------------|
| autoSaveSession              | "auto" | every `maintenanceInterval := 5000` seconds |   ✅   |      ✅      | not `"ask"`                   |
| monitorDisplayChangeMessages | "ask"  |                                             |   ✅   |      ❌      | bug.n redux must be reloaded. |

## Hotkeys/ Functions in bug.n's `Config.ini`

### Window Management
| Name                         | bug.n Comment              | bug.n | bug.n redux | bug.n redux Comment |
|:-----------------------------|:---------------------------|:-----:|:-----------:|:--------------------|
| View_activateWindow(0, +/-1) |                            |   ✅   |      ✅      |                     |
| View_shuffleWindow(0, +/-1)  |                            |   ✅   |      ✅      |                     |
| View_shuffleWindow(1)        |                            |   ✅   |      ✅      |                     |
| Manager_closeWindow()        |                            |   ✅   |      ✅      |                     |
| Window_toggleDecor()         |                            |   ✅   |      ❌      |                     |
| View_toggleFloatingWindow()  |                            |   ✅   |      ✅      |                     |
| Manager_moveWindow()         | only in manual tiling mode |   ✅   |      ❌      |                     |
| Manager_minimizeWindow()     |                            |   ✅   |      ❌      |                     |
| Manager_sizeWindow()         |                            |   ✅   |      ❌      |                     |
| Manager_maximizeWindow()     |                            |   ✅   |      ❌      |                     |
| Manager_getWindowInfo()      |                            |   ✅   |      ✅      |                     |
| Manager_getWindowList()      |                            |   ✅   |      ❌      |                     |
| View_moveWindow(0, +/-1)     | only in manual tiling mode |   ✅   |      ❌      |                     |
| Manager_maximizeWindow()     |                            |   ✅   |      ❌      |                     |
| View_moveWindow(1-10)        |                            |   ✅   |      ✅      |                     |
| View_toggleStackArea()       | only in manual tiling mode |   ✅   |      ❌      |                     |

### Window Debugging
| Name                         | bug.n Comment | bug.n | bug.n redux | bug.n redux Comment |
|:-----------------------------|:--------------|:-----:|:-----------:|:--------------------|
| Debug_logViewWindowList()    |               |   ✅   |      ❌      |                     |
| Debug_logManagedWindowList() |               |   ✅   |      ❌      |                     |
| Debug_logHelp()              |               |   ✅   |      ❌      |                     |
| Debug_setLogLevel(0, +/-1)   |               |   ✅   |      ✅      |                     |

### Layout Management
| Name                                            | bug.n Comment        | bug.n | bug.n redux | bug.n redux Comment                                |
|:------------------------------------------------|:---------------------|:-----:|:-----------:|:---------------------------------------------------|
| View_setLayout(-1/1/2/3)                        |                      |   ✅   |      ✅      | More layouts can be set.                           |
| View_setLayoutProperty("MFactor", 0, +/-0.05)   | only for tile layout |   ✅   |      ✅      |                                                    |
| View_setLayoutProperty("Axis", 0, +1/+2, 1/2/3) | only for tile layout |   ✅   |      ✅      |                                                    |
| View_setLayoutProperty("MY"/"MX", 0, +/-1)      | only for tile layout |   ✅   |      ❌      | A layout index can be set, mimicking this feature. |
| View_setLayoutProperty("GapWidth", 0, +/-2)     |                      |   ✅   |      ❌      |                                                    |
| View_resetTileLayout()                          |                      |   ✅   |      ❌      |                                                    |

### View/ Tag Management
| Name                           | bug.n Comment | bug.n | bug.n redux | bug.n redux Comment              |
|:-------------------------------|:--------------|:-----:|:-----------:|:---------------------------------|
| View_toggleMargins()           |               |   ✅   |      ❌      |                                  |
| Monitor_activateView({1-9/-1}) |               |   ✅   |      ✅      |                                  |
| Monitor_setWindowTag({1-9/10}) |               |   ✅   |      ✅      | not the value `10`, but any 'id' |
| Monitor_toggleWindowTag({1-9}) |               |   ✅   |      ❌      |                                  |

### Monitor Management
| Name                              | bug.n Comment | bug.n | bug.n redux | bug.n redux Comment                                        |
|:----------------------------------|:--------------|:-----:|:-----------:|:-----------------------------------------------------------|
| Manager_activateMonitor(0, +/-1)  |               |   ✅   |      ✅      | Each view has its own id and set of hotkeys.               |
| Manager_setWindowMonitor(0, +/-1) |               |   ✅   |      ✅      | Activating a view or tagging a window also sets a monitor. |
| Manager_setViewMonitor(0, +/-1)   |               |   ✅   |      ✅      |                                                            |

### GUI Management
| Name                                     | bug.n Comment | bug.n | bug.n redux | bug.n redux Comment |
|:-----------------------------------------|:--------------|:-----:|:-----------:|:--------------------|
| Monitor_toggleBar()                      |               |   ✅   |      ✅      |                     |
| Monitor_toggleTaskBar()                  |               |   ✅   |      ❌      |                     |
| Bar_toggleCommandGui()                   |               |   ✅   |      ❌      | no command GUI      |
| Monitor_toggleNotifyIconOverflowWindow() |               |   ✅   |      ❌      |                     |
| View_traceAreas()                        |               |   ✅   |      ❌      | no manual tiling    |

### Administration
| Name                    | bug.n Comment | bug.n | bug.n redux | bug.n redux Comment |
|:------------------------|:--------------|:-----:|:-----------:|:--------------------|
| Config_edit()           |               |   ✅   |      ✅      |                     |
| Config_UI_saveSession() |               |   ✅   |      ❌      |                     |
| Reload                  |               |   ✅   |      ✅      |                     |
| ExitApp                 |               |   ✅   |      ✅      |                     |

## Additional Features of bug.n ⭐
| Version | Description                                                                                        | bug.n Comment               | bug.n | bug.n redux | bug.n redux Comment                       |
|:--------|:---------------------------------------------------------------------------------------------------|:----------------------------|:-----:|:-----------:|:------------------------------------------|
| 9.0.1   | build script                                                                                       | tools/build.ahk             |   ✅   |      ❌      |                                           |
| 9.0.1   | tutorial                                                                                           | GitHub Wiki                 |   ✅   |      ❌      |                                           |
| 9.0.0   | control bug.n (call functions) from a nother script                                                |                             |   ✅   |      ❌      |                                           |
| 9.0.0   | minimize windows and therewith set them floating                                                   |                             |   ✅   |      ❌      |                                           |
| 9.0.0   | customizable date and time format                                                                  |                             |   ✅   |      ❌      |                                           |
| 9.0.0   | override rule for the active window by hotkey                                                      | Manager_override(rule = "") |   ✅   |      ✅      |                                           |
| 8.4.0   | manual tiling                                                                                      |                             |   ✅   |      ❌      |                                           |
| 8.4.0   | increasing mfactor resizing over time                                                              |                             |   ✅   |      ❌      |                                           |
| 8.3.0   | multi-dimensional tiling of the master area                                                        |                             |   ✅   |      ✅      | if selecting a corresponding layout index |
| 8.3.0   | logging (debugging information)                                                                    |                             |   ✅   |      ✅      |                                           |
| 8.3.0   | view margins                                                                                       |                             |   ✅   |      ❌      |                                           |
| 8.3.0   | rule based actions (macimize, close)                                                               |                             |   ✅   |      ❌      |                                           |
| 8.3.0   | view names                                                                                         |                             |   ✅   |      ✅      |                                           |
| 8.2.1   | customize reaction to acivated windows on an inactive view                                         |                             |   ✅   |      ❌      |                                           |
| 8.2.0   | set hotkeys via config.ini                                                                         |                             |   ✅   |      ✅      |                                           |
| 8.2.0   | horizontal and virtical bar position (left/ center/ right or relative in pixel, top/ bottom/ tray) |                             |   ✅   |      ✅      | not 'vertical'                            |
| 8.2.0   | customizable bar width (in pixel or %) and height (2 rows, spacious)                               |                             |   ✅   |      ✅      | not 'height'                              |
| 8.2.0   | synchronized view on all monitors                                                                  |                             |   ✅   |      ❌      |                                           |
| 8.2.0   | command input via gui                                                                              |                             |   ✅   |      ❌      |                                           |
| 8.2.0   | hotkey for opening the config file                                                                 |                             |   ✅   |      ✅      |                                           |
| 8.2.0   | setting absolute and relative values in functions (activate view, tag window, set layout)          |                             |   ✅   |      ✅      |                                           |
| 8.1.0   | maximize a window to the bug.n workspace and therewith set them floating                           |                             |   ✅   |      ❌      |                                           |

## Open Enhancement Requests from https://github.com/fuhsjr00/bug.n/issues
|   # | Description                                                                                                                               | bug.n Comment | bug.n | bug.n redux | bug.n redux Comment |
|----:|:------------------------------------------------------------------------------------------------------------------------------------------|:--------------|:-----:|:-----------:|:--------------------|
| 285 | Toggle decor of all windows of a view                                                                                                     |               |   ❌   |      ❌      |                     |
| 269 | A quick way to find & activate (hidden) window (e.g. given by title)                                                                      |               |   ❌   |      ❌      |                     |
| 259 | Two dividers with primary area in the middle (ultrawide support); also #260                                                               |               |   ❌   |      ✅      |                     |
| 254 | use bug.n only on first monitor in a dual monitor setup                                                                                   |               |   ❌   |      ❌      |                     |
| 227 | Compatibility of the bug.n status bar to more than two Windows taskbars.                                                                  |               |   ❌   |      ✅      |                     |
| 225 | Possible to rearrange items in status bar?                                                                                                |               |   ❌   |      ❌      |                     |
| 224 | New windows opened in the floating layout show title bars by default.                                                                     |               |   ❌   |      ❌      |                     |
| 224 | Forcing a window to float in tile layout shows said window's title bar.                                                                   |               |   ❌   |      ❌      |                     |
| 224 | Switching layout from floating to tiled hides open window title bars and vice versa.                                                      |               |   ❌   |      ❌      |                     |
| 222 | Use icon fonts in views name                                                                                                              |               |   ❌   |      ❔      |                     |
| 220 | Change Monitor order                                                                                                                      |               |   ❌   |      ❌      |                     |
| 211 | Show bar only when key presses                                                                                                            |               |   ❌   |      ❌      |                     |
| 201 | windows 10 virtual desktop adaptation (-> window class "ApplicationFrameWindow"!)                                                         |               |   ❌   |      ✅      |                     |
| 191 | rule based wParam handling                                                                                                                |               |   ❌   |      ❌      |                     |
| 186 | rule selection for windows owned by a specific process                                                                                    |               |   ❌   |      ✅      |                     |
| 183 | set window floating and 'always on top'                                                                                                   |               |   ❌   |      ❌      |                     |
| 162 | focus the nth window/ move the nth window to the master area; also #176                                                                   |               |   ❌   |      ❌      |                     |
| 157 | bar visibility per view                                                                                                                   |               |   ❌   |      ✅      |                     |
| 151 | i3wm's view sets per monitor/ a single set of views across all monitors                                                                   |               |   ❌   |      ✅      |                     |
| 151 | dynamic view creation and deletion                                                                                                        |               |   ❌   |      ❌      |                     |
| 151 | reset view i.e. layout properties                                                                                                         | v9.0.2        |   ✅   |      ❌      |                     |
| 151 | allow 1 hotkey -> n function calls mappings in config.ini; also #274 e.g. Move window from master to stack _and_ activate the new master. |               |   ❌   |      ❌      |                     |
| 151 | xmonad's 'greedy view'                                                                                                                    |               |   ❌   |      ❌      |                     |
| 151 | monitor-independent view -> move view to another monitor                                                                                  |               |   ❌   |      ✅      |                     |
| 136 | per monitor actions, e.g. change layout of the active view on a specific monitor; or per view actions                                     |               |   ❌   |      ✅      |                     |
| 131 | rotate window focus/ move per stack                                                                                                       |               |   ❌   |      ❌      |                     |
| 121 | xmonad's layout grid with golden ratio                                                                                                    |               |   ❌   |      ❌      |                     |
| 119 | more than one tag in a view like dwm                                                                                                      |               |   ❌   |      ❌      |                     |
| 113 | set different view names on different monitors                                                                                            |               |   ❌   |      ✅      |                     |
| 112 | more than 9 views                                                                                                                         |               |   ❌   |      ✅      |                     |
| 100 | change the height of individual windows in a vertical stack; also #270                                                                    |               |   ❌   |      ❌      |                     |
|  80 | auto-create rules for misbehaving windows                                                                                                 |               |   ❌   |      ❌      |                     |
|  48 | spread a view over more than one monitor                                                                                                  |               |   ❌   |      ❌      |                     |
|  44 | restore a session (with starting applications) or window properties (as the applications are opened by the user)                          |               |   ❌   |      ❌      |                     |
|   4 | vm and rdp suuport                                                                                                                        |               |   ❌   |      ❔      |                     |

## Closed Enhancement Requests from https://github.com/fuhsjr00/bug.n/issues
|   # | Description                                                                                     | bug.n Comment | bug.n | bug.n redux | bug.n redux Comment                                      |
|----:|:------------------------------------------------------------------------------------------------|:--------------|:-----:|:-----------:|:---------------------------------------------------------|
| 177 | regex friendly rule in windows info dialog                                                      |               |   ✅   |      ✅      |                                                          |
|  54 | doc/cheatsheet                                                                                  | v9.0.1        |   ✅   |      ❌      |                                                          |
|  42 | debugging information for rules: which rule was used for a specific window?                     |               |   ❌   |      ❌      |                                                          |
|  39 | screenshot in readme                                                                            |               |   ✅   |      ❌      |                                                          |
|  26 | allow minimizing to tray                                                                        | v9.0.0?       |   ✅   |      ✅      | if the window's status `is Tiled` is set correspondingly |
|  26 | 'the attic' view temporarily "hiding" a window                                                  |               |   ❌   |      ❌      |                                                          |
|  17 | switch to a specific monitor given by number                                                    |               |   ❌   |      ✅      |                                                          |
|  16 | display scaling aware bar width                                                                 |               |   ✅   |      ✅      |                                                          |
|  11 | read in volume level and mute status                                                            | v8.4.0        |   ✅   |      ❌      | no system information in tray                            |
|  10 | transparent bar                                                                                 | v8.4.0        |   ✅   |      ✅      |                                                          |
|   7 | (automatically) handle display changes                                                          |               |   ✅   |      ❌      | bug.n must be reloaded.                                  |
|   2 | save/ restore window properties (monitor, view, floating, ...) and bug.n settings (i.a. layout) | v8.4.0        |   ✅   |      ✅      |                                                          |
