# A Commented Version of `config.json`

``` json
  "logging": {
    "level": 4
  },
```
The logging level and therewith the type and number of messages, which are written to the log file, `%APPDATA%\bug.n\_logging.md`, can be set to a number between 1 and 5, which have the following meaning:
* 1 = CRITICAL
* 2 = ERROR
* 3 = WARNING
* 4 = INFO
* 5 = DEBUG

``` json
  "restore": {
    "layout": 1,
    "window": 1
  },
```
You can set `restore.layout` and `restore.window` to either `1` (meaning `Ttue` in this case) or `0` (meaning `false`); if set to `1`, layout and/ or window settings are restored, when reloading bug.n.

``` json
  "timer": {
    "logging": 4200,
    "objects": 4210,
    "shellDelay": 100
  },
```
Each `timer` can be set to a number in milliseconds.
* `timer.logging` gives the period, in which log messages are written to `%APPDATA%\bug.n\_logging.md`.
* `timer.objects` gives the period, in which the objects `Layout`, `View` and `Window` are written to there corresponding files `%APPDATA%\bug.n\_<object name>.md`.
* `timer.shellDelay` gives the time, bug.n waits if too many shell messages are registered and actions cannot be taken simultaniously.

## Tags/ Views

bug.n automatically defines one view for each monitor to ensure that there is at least one view and you can "focus" a monitor by activating the corresponding view. The id and label for those views are set to the index of the monitor given by the system.
``` json
  "tags": [
    [
      { "id": "q" },
      { "id": "a" },
      { "id": "y" }
    ],
    [
      { "id": "w" },
      { "id": "s" },
      { "id": "x" }
    ],
    [
      { "id": "d" },
      { "id": "c" }
    ]
  ],
```
Additional tags corresponding to views and therewith layouts can be given as an array of tags per monitor. Therefor the following array defines the additional views for monitor 1:
``` json
    [
      { "id": "q" },
      { "id": "a" },
      { "id": "y" }
    ]
```
This results in {1, Q, A, Y} being the available views on monitor 1.

Each tag is defined by the following keys:
* **id**: A unique ID, which corresponds to the hotkeys associated with the tag.
* **label**: The label, which is shown in the tray, indicating the view; if not given, it is derived from the ID by making it upper case and enclosing it with space on each side.
* **layouts**: An array of layouts and indices as described below under the topic "Layouts" for `layout.defaults`. It defines the layouts, which can be set on this view, and the default index. If not given, `layout.defaults` is assumed.

Therewith the first tag/ view on monitor one would be the following, if explicitly wrote down:
``` json
{ "id": "q", "label": " Q ", "layouts": [
                              [ "monocle",  "*" ],
                              [ "binocle",  "*" ],
                              [ "tile",     "*" ],
                              [ "hinocle",  "*" ],
                              [ "tileH",    "*" ],
                              [ "dropovl",  "*" ],
                              [ "floating", "*" ]] }
```
You may e.g. want to give a more verbose label.

## Tray

``` json
  "tray": {
    "colors": {
      "aView":  { "back": "000000", "font": "d46a35", "fore": "d46a35" },
      "eView":  { "back": "000000", "font": "707070", "fore": "000000" },
      "layout": { "back": "353535", "font": "ffffff", "fore": "000000" },
      "views":  { "back": "000000", "font": "ffffff", "fore": "000000" },
      "vView":  { "back": "000000", "font": "356ad4", "fore": "000000" },
      "window": { "back": "000000", "font": "ffffff", "fore": "000000" }
    },
    "font": {
      "name": "Lucida Console",
      "size": 10
    },
    "horizontalPos": "center",
    "mark": {
      "aView": "vertical 5",
      "layout": "100"
    },
    "relation": "parent",
    "transparency": "Off",
    "visible": [1]
  },
```
The tray is bug.n's GUI. There is one on each monitor, set by default in the Windows Taskbar, indicating the active and available views and the active layout on each monitor.

The position of the tray inside the Taskbar can be set by `tray.horizontalPos`, choosing one of the set {`left`, `center`, `right`} or giving a number of pixels (positiv value: from the left, negative value: from the right side of the monitor).

With `tray.relation` the appearance and behaviour of the tray window in relation to the Taskbar can be modified by setting one of the following values:
* `parent`: The Taskbar is set as the parent window of the tray, fully integrating it in the Taskbar; this may be problematic, if using light colors for the Taskbar.
* `owner`: The Taskbar is set to be the owner of the tray window resulting in an 'always on top' overlay to the Taskbar with clear colors, but which has to be manually hidden, if using any window in fullscreen mode.

You can set the visibility of the tray for each monitor with `tray.visible`, an array consisting of values `1` (`true`/ visible) or `0` (`false`, not visible) for each monitor; if not given, the value is assumed to be `1` making the tray visible by default.

You may also set a transparancy value for the whole window, by giving a number between 0 and 255 (with 0 making the window invisible and 255 making it opaque).

With `tray.colors` you can set the background, foreground and font color for the following elements/ status:
* `aView`: The active view.
* `eView`: An empty view (with no windows).
* `vView`: A visible, but not active view. In a multi-monitor system you may have the active view (with the active window) and additional monitors with a view visible, as they do not overlap with the active view.
* `views`: The default color of a view (not active, not visible, but with windows).
* `layout`: The label of the active layout on the right side of the tray.
* `window`: The GUI window itself, mostly relevant for the background color.

Additionally to the font color for each tray element/ status, you can set the font family and size (one for all trays and elements).

You may also set a 'mark' for the active view and/ Or layout, which can be a left bar (with the value set to e.g. `5`), a bottom bar (with the value set to e.g. `vertical 5`) or a frame (with the value set to `100`). The color of a 'bar' (left or bottom) is defined by `tray.colors.*.fore`, whilst the color of a 'frame' is defined by `tray.colors.*.back` and the 'background' color by `tray.colors.*.fore`.

## Hotkeys

``` json
  "hotkeys": {
    "WinCtrle": {
      "function": "Main_editConfigFile",
      "arguments": []
    },
    "WinCtrlr": {
      "function": "Reload"
    },
    "Winm": {
      "function": "Layout_set",
      "arguments": ["array", "monocle"]
    },
    "WinCtrlBackSpace": {
      "function": "Layout_set",
      "arguments": ["index", "*", "0"]
    },
    "WinUp": {
      "function": "View_activateWindow",
      "arguments": ["-1", "1"]
    },
    "Win1": {
      "function": "View_activate",
      "arguments": ["1"]
    },
  },
```

Hotkeys and therewith how bug.n is controlled can be set by giving the hotkey itself (the key combination) and the associated function and arguments. The ones given above are only a selection. You can find a full list of the [default hotkeys](./default-hotkeys.md) in the documentation.

A hotkey (e.g. `"WinCtrle"` in the first entry above) consists of the modifier keys `Win`, `Alt`, `Ctrl`, `Shift` (in the example only `Win` and `Ctrl`) and one additional key (in the example `e`). You may often find a notation using ` + ` in between the keys (e.g `Win + Ctrl + E`); here the hotkey is written without any `+`, space or capitalization of the final key.

In some cases you may change the arguments of a function (e.g. exchanging `+1` with `-1`) or you may add your own hotkeys for more differentiated control or for running programs like in the following example:
``` json
"Wine": {
  "function": "Target",
  "arguments": ["\"C:\\Windows\\explorer.exe\"", ""]
}
```

## Layouts

``` json
  "layout": {
    "arrays": {
      "binocle": {
        "layouts": [
          {
            "label": "[ %n% ]",
            "minWin": 1,
            "stacks": [
              { "h": "1.0", "maxWin": 0, "pd": 3, "w": "1.0", "x": "0.0", "y": "0.0" }
            ]
          },
          {
            "label": "[1|%n%]",
            "minWin": 2,
            "p": "0.5",
            "q": "0.5",
            "stacks": [
              { "h": "1.0", "maxWin": 1, "pd": 3, "w": "p", "x": "0.0", "y": "0.0" },
              { "h": "1.0", "maxWin": 0, "pd": 3, "w": "q", "x": "p", "y": "0.0" }
            ]
          },
          {
            "label": "[2|%n%]",
            "minWin": 3,
            "p": "0.5",
            "q": "0.5",
            "stacks": [
              { "h": "1.0", "maxWin": 2, "pd": 3, "w": "p", "x": "0.0", "y": "0.0" },
              { "h": "1.0", "maxWin": 0, "pd": 3, "w": "q", "x": "p", "y": "0.0" }
            ]
          }
        ]
      }
    },
    "defaults": [
      [ "monocle",  "*" ],
      [ "binocle",  "*" ],
      [ "tile",     "*" ],
      [ "hinocle",  "*" ],
      [ "tileH",    "*" ],
      [ "dropovl",  "*" ],
      [ "floating", "*" ]
    ]
  },
```

(Dynamic) Layouts are defined as arrays of specific layouts/ arrangements. The above is one example, where the key, `"binocle"`, is the identifier and the array `"layouts"` describes the specific arrangements. At the `n`th index should be a layout, which is able to hold at least `n` windows; therefor at index `1` should always be the specific layout `monocle`. (This specific layout spans the whole monitor work area and shows one window at a time.)

A specific layout can be described with the following information:
* `minWin`: The number of windows, which the layout is able to hold.
* `stacks`: An array of stacks, which do the actual stacking/ arrangment of windows.
* `p`: A 'variable' giving a factor, which can be used for describing the position and dimensions of stacks, and later can be de-/incremented by the function `Layout_set` with the first argument `pq` therewith altering the layout.
* `q`: The analog to `p`; `p` and `q` should add up to `1`.
* `label`: The label is shown in the tray as the most right element, if the layout is active. It may contain the 'variable' `%n%`, which is replaced by the number of windows in the stack with `maxWin: 0`.

A stack is defined by the following information:
* `maxWin`: The maximum number of windows, which should be placed in the stack. If it is set to `0`, the remaining windows, which should be tiled, are placed in that stack. The stack with `maxWin: 0` should be the last in the array of stacks.
* `x`: The x-coordinate of the stack relative to the position and width of the monitor's work area. The value should be between `0.0` and `1.0` and can be set to the variable `p` or `q`, which will be replaced by the corresponding value of the layout above.
* `y`: The y-coordinate of the stack analog to `x` and relative to the height of the monitor's work area.
* `w`: The stack's width with the same options as `x`.
* `h`: The stack's height with the same options as `y`.
* `pd`: The propagation direction of the stack, which later can be 'rotated' by the function `Layout_set` with `pd` as the first argument. It can have the following values:
  * `1`: Arranging windows along the x-axis, from left to right.
  * `2`: Arranging windows along the y-axis, from top to bottom.
  * `3`: Windows are stacked one on top of the other, with one window visible at any time.

## WIndow Rules

``` json
  "window": {
    "rules": [
      {
        "class": "^ApplicationFrameWindow$",
        "procPath": "^C:\\\\Windows\\\\System32\\\\ApplicationFrameHost\\.exe$",
        "title": "^3D-Viewer|Calculator|Rechner|Camera|Kamera|Maps|Karten|Microsoft Store|Paint 3D|Photos|Fotos|Settings|Einstellungen|Windows Security|Windows-Sicherheit$",
        "isManaged": 1, "isTiled": 1, "tag": ""
      },
      {
        "class": "^CabinetWClass$",
        "hasCaption": 1,
        "isChild": 0,
        "isElevated": 0,
        "isGhost": 0,
        "isPopup": 0,
        "minMax": "0|1|-1",
        "procPath": "^C:\\\\Windows\\\\explorer\\.exe$",
        "title": ".+",
        "visible": 1,
        "isManaged": 1, "isTiled": 1, "tag": ""
      },
      {
        "class": "^Chrome_WidgetWin_1$",
        "procPath": "^C:\\\\Program Files.*\\\\Microsoft\\\\Edge\\\\Application\\\\msedge\\.exe$",
        "title": ".+ Microsoft.* Edge$",
        "isManaged": 1, "isTiled": 1, "tag": ""
      },
    ]
  }
```

For handling windows bug.n applies a set of rules given by `windows.rules`. Each rule consists of two parts: (1) Identifying the window and (2) seeting the window's properties. For identifiying the window the following information can be used and is matched against a regular expression:
* `class`: The window's class name.
* `hasCaption`: A boolean value (either `1` for `true` or `0` for `false`); does the window has a caption/ window title bar?
* `isChild`: A boolean value (either `1` for `true` or `0` for `false`); is the window a child of another window?
* `isElevated`: A boolean value (either `1` for `true` or `0` for `false`); has the process, the window is associated with, administrative privileges?
* `isGhost`: A boolean value (either `1` for `true` or `0` for `false`); is the process, the window is associated with, not responding and the window only a 'shadow of itself'?
* `isPopup`: A boolean value (either `1` for `true` or `0` for `false`); is the window a pop-up?
* `minMax`: A value from the set {-1, 1, 0} indicating one of the status from the set {'minimized', 'maximized', 'neither'}.
* `procPath`: The file path of the process, the window is associated with. Especially characters like `\` and `"` must be escaped by `\` to conform with the js object notation; additionally characters have to be escaped regarding regular expressions.
* `title`: The window's title; escaping characters is needed as with `procPath`.
* `visible`: A boolean value (either `1` for `true` or `0` for `false`); is the window visible?

The following window properties, which are used by bug.n, can be set:
* `isManaged`: A boolean value (either `1` for `true` or `0` for `false`); should bug.n manage the window?
* `isTiled`: A boolean value (either `1` for `true` or `0` for `false`); should the window be tiled and placed in a layout/ stack?
* `tag`: One of the tags, as described above under "Tags/ Views" and defined by `tags`, the window should be associated with.

bug.n applies the rules to newly found windows in the order given in `window.rules` (from top to bottom); is the identifying part of a rule matches the window, the given properties are set and no further rules are applied.

The rules given above are only an examplary subset of the rules given in `config.json`. The second rule (for the Windows Explorer) is the long version with all information, which is checked by bug.n; the second rule is a short version as it is often sufficient to identifiy a window by `class`, `procPath` and `title`. If not given, an identifying window property is not checked (evaluates to 'match').

You can see the properties, bug.n accumulated for a window, by pressing the hotkey _Win + Ctrl + W_. And you can get a first attempt for a short rule for copying to the clipboard by pressing the hotkey _Win + Ctrl + Shift + W_.

The rule set in `config.json` is a positive list for windows, which should be managed by bug.n. You may also set a default rule as the last one and make exemptions before that for windows you do not want to be managed by bug.n; e.g.:
``` json
  "window": {
    "rules": [
      {
        "class": "^Shell_TrayWnd$",
        "procPath": "^C:\\\\Windows\\\\explorer\\.exe$",
        "title": "^$",
        "isManaged": 0, "isTiled": 0, "tag": ""
      },
      {
        "class": "^Progman$",
        "procPath": "^C:\\\\Windows\\\\explorer\\.exe$",
        "title": "^Program Manager$",
        "isManaged": 0, "isTiled": 0, "tag": ""
      },
      {
        "class": "^MultitaskingViewFrame$",
        "procPath": "^C:\\\\Windows\\\\explorer\\.exe$",
        "title": ".*",
        "isManaged": 0, "isTiled": 0, "tag": ""
      },
      {
        "class": ".+",
        "isGhost": 0,
        "isPopup": 1,
        "procPath": ".+",
        "title": ".*",
        "visible": 1,
        "isManaged": 1, "isTiled": 0, "tag": ""
      },
      {
        "class": ".+",
        "isGhost": 0,
        "procPath": ".+",
        "title": ".*",
        "visible": 1,
        "isManaged": 1, "isTiled": 1, "tag": ""
      },
    ]
  }
```
But be aware, that you may catch windows accidentally!