# The Default Hotkeys

| Hotkey                     | Function/ Command                  | Description |
|:-------------------------- |:---------------------------------- |:----------- |
| Win + Shift + Space        | Tray_toggleVisibility()            | Hide/ Show bug.n's GUI in the Windows' Taskbar. This may help, if the GUI covors Taskbar icons. |
| Win + Ctrl + e             | Main_editConfigFile()              | Open bug.n's configuration file, `config.json`, either from `%APPDATA%\bug.n`, or bug.n's `src` directory. It is opened in the editor, which is associated with the file extension `.json`. |
| Win + Ctrl + q             | ExitApp                            | Quit bug.n. Hidden windows from not active views are made visible. |
| Win + Ctrl + r             | Reload                             | Restart bug.n. If `restore.layout` and/ or `restore.window` is set in `config.json`, the corresponding status are restored. |
| Win + Ctrl + x             | Window_close()                     | Close the activ window, if it is managed by bug.n. |
| Win + Ctrl + w             | Window_writeObjectToClipboard()    | Pop up a message window with the active window's object as text. If selected, this is copied to the clipboard. |
| Win + Ctrl + Shift + w     | Window_writeShortRuleToClipboard() | Pop up a message window with the active window's object reduced to a rule usable for inserting in `config.json`. If selected, this is copied to the clipboard. |
| Win + b                    | Layout_set("array", "binocle")     | Set the layout of the active view to `binocle` (`[ | ]`), which shows to windows side by side. Depending on the number of windows on that view, the windows are laid out. If there is only one window tiled on the view, it is laid out with the `monocle` layout. |
| Win + h                    | Layout_set("array", "hinocle")     | Set the layout of the active view to `hinocle` (`------`), which is `binocle` in horizontal orientation. |
| Win + m                    | Layout_set("array", "monocle")     | Set the layout of the active view to `monocle` (`[   ]`), which shows one window at a time maximized to the monitor's work area. |
| Win + n                    | Layout_set("array", "tileH")       | Set the layout of the active view to `tileH` (`[ ]|`), which shows the first window on the left half of the view and max. two windows side by side on the right half. If there are more than three windows tiled on the view, they are laid out like in the `binocle` layout. |
| Win + p                    | Layout_set("array", "dropovl")     | Set the layout of the active view to `dropovl` (`[  *]`), which shows the first window in the top right corner and all others like in the `monocle` layout. |
| Win + t                    | Layout_set("array", "tile")        | Set the layout of the active view to `tile` (`[ ]=`), which shows the first _n_ windows in a 'main' area on the left and the rest stacked vertically on the right. If the total number of windows in the right stack is greater than three, the number of windows in the 'main' area is increased, resulting in a grid like layout. |
| Win + u                    | Layout_set("array", "floating")    | Set the layout of the active view to `floating` (`><>`), not tiling any windows. |
| Win + Tab                  | Layout_set("array", "-1")          | Set the layout of the active view to the one next to last. |
| Win + Ctrl + BackSpace     | Layout_set("index", "*", "0")      | Reset the layout index to adapt to the number of windows tiled on the view as made possible by the layout array. |
| Win + Ctrl + Left          | Layout_set("index", "-1", "1")     | Decrement the layout index resulting in a layout having less stacks. |
| Win + Ctrl + Right         | Layout_set("index", "+1", "1")     | Increment the layout index resulting in a layout having more stacks. |
| Win + Ctrl + Tab           | Layout_set("pd", "+1", "1")        | Change the propagation direction of the active window's stack from 1 (x/ horizontal) to 2 (y/ vertical) to 3 (z/ deck/ monocle). |
| Win + Shift + Left         | Layout_set("pq", "-0.02", "1")     | Decrement the p-q-factor of a layout with at least two stacks, e.g. making the left stack narrower and the right wider. |
| Win + Shift + Right        | Layout_set("pq", "+0.02", "1")     | Increment the p-q-factor of a layout with at least two stacks, e.g. making the left stack wider and the right narrower. |
| Win + Shift + Tab          | Layout_shuffleStack("1")           | Move the active window's stack to the first position in the stack array, e.g. moving the stack to the left. |
| Win + Ctrl + Shift + Enter | Layout_toggleWindow()              | Toggle the window's `isTiled` status, if it is associated with a view (tagged), adding it to or removing it from the layout. |
| Win + Ctrl + Enter         | View_toggleWindow()                | Toggle the window's `isManaged` status adding it to (or removing it from) a view. This has to be done before `Layout_toggleWindow` can have an effect. |
| Win + Shift + Enter        | View_shuffleWindow("1", "0")       | Move the active window to the first position in the view's array, e.g. implicitly moving it to the first position in a layout, if the window's status `isTiled`. |
| Win + Shift + Down         | View_shuffleWindow("+1", "1")      | Move the active window to the next position further down in the view's stack, e.g. moving it down in a stack. |
| Win + Shift + Up           | View_shuffleWindow("-1", "1")      | Move the active window to the next position further up in the view's stack, e.g. moving it up in a stack. |
| Win + Down                 | View_activateWindow("+1", "1")     | Activate the next window to the currently active window further down in the view's stack. |
| Win + Up                   | View_activateWindow("-1", "1")     | Activate the next window to the currently active window further up in the view's stack. |
| Win + BackSpace            | View_activate("-1")                | Activate the view, which was active before the currently active one. |
| Win + 1                    | View_activate("1")                 | Activate the view with id '1', implicitly activating the first monitor, since a view with id `n` corresponds with monitor `n`. |
| Win + `n`                  | View_activate("`n`")               | Activate the view with id '`n`' (with `n` from the set {1, 2, 3, 4, 5, 6, 7, 8, 9}). A numbered view can only be activated, if it exists, i.e. if there is a corresponding monitor. |
| Win + `id`                 | View_activate("`id`")              | Activate the view with id '`id`' (with `id` from the set {q, a, y, w, s, x, d, c}). |
| Win + Shift + 1            | View_moveWindow("1")               | Move the active window to the view with id '1'. |
| Win + Shift + `n`          | View_moveWindow("`n`")             | Move the active window to the view with id `n` corresponding to monitor `n` (with `n` from the set {1, 2, 3, 4, 5, 6, 7, 8, 9}). |
| Win + Shift + `id`         | View_moveWindow("`id`")            | Move the active window to the view with id `id` (with `id` from the set {q, a, y, w, s, x, d, c}). |
| Alt + Shift + 1            | View_move("1")                     | Move the active view to monitor '1'. |
| Alt + Shift + `n`          | View_move("`n`")                   | Move the active view to monitor `n` (with `n` from the set {1, 2, 3, 4, 5, 6, 7, 8, 9}). |
| Alt + Ctrl + Shift + `n`   | View_moveAll("`n`")                | Move all views to monitor `n` (with `n` from the set {1, 2, 3, 4, 5, 6, 7, 8, 9}). |
