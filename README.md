## bug.n &ndash; Tiling Window Management for Microsoft Windows

bug.n is a [tiling window management](https://en.wikipedia.org/wiki/Tiling_window_manager) add-on for the Explorer shell of Microsoft Windows. 
It is written in the scripting language [AutoHotkey](https://www.autohotkey.com/download/).

This fork removes some non-essential features &ndash; as defined by the author &ndash; to reduce code complexity and improve the possibility to maintain the code and fix bugs. You may find a list comparing the [features of bug.n redux](./doc/feature-comparison.md) and bug.n (v9) in the documentation.

### Installing and Running bug.n

You may [download the current development version](https://codeberg.org/joten/bug.n/archive/master.zip) as the repository itself. 
With which you will have a `zip` file including the source (`src\*`) files.

There is no installation process for bug.n. 
Unpack the `zip` file, and you should be able to run the main script (`src\main.ahk`) with [AutoHotkey](https://www.autohotkey.com/download/), 
if your system meets the requirements.

#### Requirements

* Technically: Microsoft Windows 2000 or higher; for security reasons or if you want to use virtual desktops for hiding windows: Microsoft Windows 10 or higher!
* [AutoHotkey](https://www.autohotkey.com/download/) 1.1.27 or higher
* [Visual C++ redistributable packages for Visual Studio 2017 (vc_redist.x64.exe)](https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads), if you want to use virtual desktops for hiding windows

### Usage and Configuration

bug.n is controlled by keyboard. You may find a list of the [default hotkeys](./doc/default-hotkeys.md) in the documentation. It features a GUI indicating the available views/ tags and the active view/tag and layout.

bug.n is configured with a JSON file, `config.json`. First it searches for this file in the user's app data directory, `%APPDATA%\bug.n`, then in the `src` directory of bug.n. You may find a [commented version](./doc/config.json.md) of this file in the documentation.

### License

bug.n is licensed under the GNU General Public License version 3. Please see the [LICENSE file](./LICENSE.md) for the full license text.

### Development

This is experimental software and currently in development.
