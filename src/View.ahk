/*
:title:     bug.n - tiling window management
:copyright: (c) 2022 joten <https://codeberg.org/joten>
:license:  GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.txt)

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

View__init(filename := "") {
  Global Config, Main, View

  View := {aView: {now: "1", before: "1"}
         , cache: {}
         , filename: filename}
  
  For i, item in Main.tags {                    ;; item: monitor
    For j, jtem in item {                       ;; jtem: view
      View.cache[jtem.id] := View_getObject(jtem.id, i)
      View.cache[jtem.id].visible := (j == 1)   ;; At start only the first view on each monitor is visible; it is not restored from `object.cache` below.
    }
  }
  
  If (Config.restore.window) {
    FileRead, json, % filename
    object := Jxon_Load(json)
    View.aView.before := (View.cache.HasKey(object.aView.now) ? object.aView.now : (View.cache.HasKey(object.aView.before) ? object.aView.before : "1"))
    For key, value in object.cache {
      If (View.cache.HasKey(key)) {
        ;; If the tag/ view still exists after reloading the script, the window list and active window is restored.
        View.cache[key].aWindow.now := (WinExist("ahk_id " . value.aWindow.now) ? value.aWindow.now : "")
        View.cache[key].aWindow.before := (WinExist("ahk_id " . value.aWindow.before) ? value.aWindow.before : "")
        For i, item in value.windows {
          If (WinExist("ahk_id " . item)) {
            View.cache[key].windows.Push(item)
            View.cache[key].indices[item] := View.cache[key].windows.Length()
          }
        }
      }
    }
  }
  
  Logging_info("Initialized ``" . View.cache.Count() . "`` view" . (View.cache.Count() == 1 ? "" : "s") . ".", "View__init")
}

View_activate(id) {
  Global Main, View, Window

  If (id == "-1") {
    id := View.aView.before
  }
  If (View.cache.HasKey(id) && id != View.aView.now) {
/*  ;; Do the following four lines provide redundant functionality?
    winId := Format("0x{:x}", WinExist("A"))
    If (Window.cache[winId].tag == View.aView.now) {
      View.cache[View.aView.now].aWindow.now := winId
    }
*/
    For i, item in Main.tags[View.cache[id].monitor] {
      If (item.id != id && View.cache[item.id].visible) {
        View_run(item.id, "hide")
      }
    }
    View.aView.before := (View.aView.now != "" ? View.aView.now : id)
    View.aView.now := id
    View_run(id, "show")
    Layout_arrange(id)
    Window_run(View.cache[id].aWindow.now, "activate")
    Tray_updateViews(View.cache[View.aView.before].monitor, [View.aView.before])
    Tray_updateViews(View.cache[id].monitor, [id])
  }
}

View_activateWindow(value, delta := False) {
  Global View, Window

  id := View.aView.now
  winId := Format("0x{:x}", WinExist("A"))
  If (Window.cache[winId].tag == id && View.cache[id].aWindow.now != winId) {
    View.cache[id].aWindow.before := View.cache[id].aWindow.now
    View.cache[id].aWindow.now := winId
  }
  If (delta) {
    value += View_getWindowIndex(id, View.cache[id].aWindow.now)
  }
  N := View.cache[id].windows.Length()
  value := (value < 1) ? N : (value > N ? 1 : value)    ;; loop, if out of bound
  Window_run(View.cache[id].windows[value], "activate")
}

View_getObject(id, monitor) {
  Return, {aWindow: {now: "", before: ""}
         , id: id
         , indices: {}
         , monitor: monitor
         , visible: False
         , windows: []}
}

View_getWindowIndex(id, winId) {
  Global View

  index := 0
  For i, item in View.cache[id].windows {
    View.indices[item] := i
    If (item == winId) {
      index := i
    }
  }
  Return, index
}

View_insertWindow(id, winId) {
  Global View, Window

  If (View.cache.HasKey(id) && Window.cache.HasKey(winId) && !View.cache[id].indices.HasKey(winId)) {
    View.cache[id].windows.Push(winId)
    View.cache[id].indices[winId] := View.cache[id].windows.Length()
    View.cache[id].aWindow.before := View.cache[id].aWindow.now
    View.cache[id].aWindow.now := winId
    Tray_updateViews(View.cache[id].monitor, [id])
  }
}

View_move(index) {
  Global Main, View

  If (index <= Main.tags.Length() && View.aView.now != View.cache[View.aView.now].monitor && index != View.cache[View.aView.now].monitor) {
    ;; Is the target `index` in the range of monitors? Is th active view (to be moved) not a monitor related (numbered) view? Is the target monitor not the current monitor?
    Logging_info("Moving view ``" View.aView.now . "`` from monitor **" . View.cache[View.aView.now].monitor . "** to **" . index . "**.", "View_move")

    ;; Remove the active view from the current monitor.
    tag := ""
    For i, item in Main.tags[View.cache[View.aView.now].monitor] {
      If (item.id == View.aView.now) {
        tag := Main.tags[View.cache[View.aView.now].monitor].RemoveAt(i)
        Logging_debug("Removed view ``" View.aView.now . "`` at position **" . i . "** from monitor **" . View.cache[View.aView.now].monitor . "**.", "View_move")
        Break
      }
    }

    ;; Add the active view to the target monitor -- only if it has been found and removed previously.
    If (tag.id == "") {
      Logging_error("Active view ``" View.aView.now . "`` not found on monitor **" . View.cache[View.aView.now].monitor . "**.", "View_move")
    } Else {
      View.cache[View.aView.now].monitor := index
      Main.tags[index].Push(tag)
      For i, item in Main.tags[index] {
        If (item.id != tag.id) {
          View.cache[item.id].visible := False
        }
      }
      Tray__init()
      Main_registerShellHook()
      Layout_arrange(View.aView.now)
    }
  }
}

View_moveAll(index) {
  Global Main, View

  If (index <= Main.tags.Length()) {
    ;; Is the target `index` in the range of monitors?
    Logging_info("Moving all views to monitor **" . index . "**.", "View_moveAll")

    ;; Remove views from the current monitor.
    tags := []
    For i, item in Main.tags {    ;; For each monitor, 
      If (i != index) {           ;; which is not the target, 
        n := item.Length()
        Loop, % n {               ;; remove all views, 
          j := n - A_Index + 1
          jtem := item[j]
          If (jtem.id != i) {     ;; which are not monitor related (numbered).
            tags.InsertAt(1, Main.tags[i].RemoveAt(j))
            Logging_debug("Removed view ``" jtem.id . "`` at position **" . j . "** from monitor **" . i . "**.", "View_moveAll")
          }
        }
      }
    }

    ;; Add the previously removed views to the target monitor.
    For i, item in tags {
      View.cache[item.id].monitor := index
      Main.tags[index].Push(item)
    }
    tags := ""
    For i, item in Main.tags[index] {
      If (item.id != View.aView.now) {
        View.cache[item.id].visible := False
      }
      tags .= (i > 1 ? ", " : "") . "``" . item.id . "``"
    }
    Logging_debug("The following views are now on monitor **" . index . "**: " . tags . ".", "View_moveAll")
    Tray__init()
    Main_registerShellHook()
    Layout_arrange(View.aView.now)
  }
}

View_moveWindow(id) {
  Global Main, View, Window

  winId := Format("0x{:x}", WinExist("A"))
  If (Window.cache.HasKey(winId) && Window.cache[winId].isManaged) {
    wnd := Window.cache[winId]
    If (View.cache.HasKey(id) && wnd.tag != id) {
      ;; `Main.onHideShow` must be set before `View_removeWindow`, not just before `Window_run(wnd.id, "hide")`
      ;; to prevent `Layout_arrange` triggering `Main_onShellMessage` and re-inserting the window.
      Main.onHideShow := True
      View_removeWindow(wnd.tag, wnd.id)
      If (wnd.isTiled) {
        Layout_arrange(wnd.tag)
      }
      Window_run(View.cache[wnd.tag].aWindow.now, "activate")
      View_insertWindow(id, wnd.id)
      wnd.tag := id
      If (!View.cache[id].visible) {
        Window_run(wnd.id, "hide")
      } Else {
        Layout_arrange(id)
      }
      Main.onHideShow := False
    }
  }
}

View_removeWindow(id, winId) {
  Global View, Window

  If (View.cache.HasKey(id) && Window.cache.HasKey(winId)) {
    index := View_getWindowIndex(id, winId)
    If (index > 0) {
      View.cache[id].windows.RemoveAt(index)
      View.cache[id].indices.Delete(winId)
      If (winId == View.cache[id].aWindow.now) {
        View.cache[id].aWindow.now := View.cache[id].aWindow.before
        View.cache[id].aWindow.before := ""
      } Else If (winId == View.cache[id].aWindow.before) {
        View.cache[id].aWindow.before := ""
      }
      View_setActiveWindow(id)
      Tray_updateViews(View.cache[id].monitor, [id])
    }
  }
}

View_run(id, command) {
  Global Main, View

  If (RegExMatch(command, "hide|show")) {
    ;; Try to hide the disappearance of windows behind the active one to minimize flickering.
    Window_run(View.cache[id].aWindow.now, "top")
    ;; Accelerate the following 
    WD := A_WinDelay
    SetWinDelay, 0
    Main.onHideShow := True
  }
  For i, item in View.cache[id].windows {
    ;; Leave the active window for last to disappear.
    If (command != "hide" || item != View.cache[id].aWindow.now) {
      Window_run(item, command)
    }
  }
  If (RegExMatch(command, "hide|show")) {
    ;; Catching up.
    If (command == "hide") {
      Window_run(View.cache[id].aWindow.now, command)
    }
    Main.onHideShow := False
    SetWinDelay, % WD
    Window_run(View.cache[id].aWindow.now, "top")
    
    View.cache[id].visible := (command == "show")
    If (command == "hide") {
      Tray_updateViews(View.cache[id].monitor, [])
    }
  }
}

View_setActiveWindow(id) {
  Global View

  If (View.cache[id].aWindow.now == "" && View.cache[id].windows.Length() > 0) {
    If (View.cache[id].aWindow.before != "") {
      View.cache[id].aWindow.now := View.cache[id].aWindow.before
    } Else {
      View.cache[id].aWindow.now := View.cache[id].windows[1]
    }
  }
  If (View.cache[id].aWindow.before == "" && View.cache[id].windows.Length() > 1) {
    For i, item in View.cache[id].windows {
      If (item != View.cache[id].aWindow.now) {
        View.cache[id].aWindow.before := item
        Break
      }
    }
  }
}

View_shuffleWindow(value, delta := False, mustBeTiled := False) {
  Global View, Window

  winId := Format("0x{:x}", WinExist("A"))
  If (Window.cache.HasKey(winId) && Window.cache[winId].tag != "") {
    id := Window.cache[winId].tag
    index := View_getWindowIndex(id, winId)
    If (delta) {
      value += index
    } Else If (value == 1 && index == 1) {
      value := 2
    }
    N := View.cache[id].windows.Length()
    value := (value < 1) ? N : (value > N ? 1 : value)
    View.cache[id].windows.RemoveAt(index)
    View.cache[id].windows.InsertAt(value, winId)
    Layout_arrange(id)
  }
}

View_toggleWindow() {
  Global View, Window

  winId := Format("0x{:x}", WinExist("A"))
  If (Window.cache.HasKey(winId)) {
    wnd := Window.cache[winId]
    wnd.isManaged := !wnd.isManaged
    If (wnd.isManaged) {
      wnd.tag := View.aView.now
      View_insertWindow(wnd.tag, wnd.id)
      If (!wnd.isTiled) {
        ;; If the window is floating, set the layout symbol corresponding to the active window.
        Tray_updateLayout(View.cache[wnd.tag].monitor, "~")
      }
    } Else {
      View_removeWindow(wnd.tag, wnd.id)
    }
  }
}
