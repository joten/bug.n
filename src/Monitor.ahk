/*
:title:     bug.n - tiling window management
:copyright: (c) 2022 joten <https://codeberg.org/joten>
:license:  GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.txt)

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

Monitor__init(filename := "") {
  Global Monitor

  Monitor := {cache: []
            , filename: filename
            , indices: {}
            , primary: 0}

  ;; Set DPI awareness.
  DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2 := -4
  result := DllCall("User32\SetThreadDpiAwarenessContext", "Int" , DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2)
  ;; pneumatic: -DPIScale not working properly (https://www.autohotkey.com/boards/viewtopic.php?p=241869&sid=abb2db983d2b3966bc040c3614c0971e#p241869)
  ;; InnI: Get per-monitor DPI scaling factor (https://www.autoitscript.com/forum/topic/189341-get-per-monitor-dpi-scaling-factor/?tab=comments#comment-1359832)
  Logging_debug("Dll _User32\SetThreadDpiAwarenessContext_ called with result ``" . Format("0x{:x}", result) . "``.", "Monitor__init")
  
  ;; Enumerate and synchronizing displays/ AutoHotkey monitors.
  Monitor_enumAutoHotkeyMonitors()
  DllCall("EnumDisplayMonitors", "Ptr", 0, "Ptr", 0, "Ptr", RegisterCallback("Monitor_enumDisplayMonitorsProc", "", 4), "UInt", 0)
  ;; Solar: SysGet incorrectly identifies monitors (https://autohotkey.com/board/topic/66536-sysget-incorrectly-identifies-monitors/)
  
  Logging_info("Initialized ``" . Monitor.cache.Length() . "`` monitor" . (Monitor.cache.Length() == 1 ? "" : "s") . ".", "Monitor__init")
}

Monitor_enumAutoHotkeyMonitors() {
  ;; Enumerate the monitors as found by AutoHotkey.
  Global Monitor
  
  SysGet, n, MonitorCount
  Logging_debug("``" . n . "`` monitor" . (n == 1 ? "" : "s") . " found by _AutoHotkey_.", "Monitor_enumAutoHotkeyMonitors")
  Loop, % n {
    SysGet, name, MonitorName, % A_Index
    SysGet, rect, Monitor, % A_Index
    
    Monitor_insert(A_Index, rectLeft, rectTop, rectRight, rectBottom)

    ;; Supplementing information, getting the work area and finding the Windows Taskbar.
    Monitor.cache[A_Index].ahkIndex := A_Index
    Monitor.cache[A_Index].name := name
    Monitor.cache[A_Index].shellTrayWnd := Monitor_getShellTrayWnd(rectLeft, rectTop, rectRight, rectBottom)
    SysGet, rect, MonitorWorkArea, % A_Index
    Monitor.cache[A_Index].workArea := {x: rectLeft, y: rectTop, w: rectRight - rectLeft, h: rectBottom - rectTop}
  }
  SysGet, i, MonitorPrimary
  Monitor.cache[i].isPrimary := True
  Monitor.primary := i
}

Monitor_enumDisplayMonitorsProc(hMonitor, hdcMonitor, lprcMonitor, dwData) {
  ;; Appending additional monitors not previously found by AutoHotkey,
  ;; synchronizing indices and supplementing information to screen objects.
  Global Monitor

  handle := Format("0x{:x}", Abs(hMonitor))
  rectLeft    := NumGet(lprcMonitor + 0,  0, "Int")
  rectTop     := NumGet(lprcMonitor + 0,  4, "Int")
  rectRight   := NumGet(lprcMonitor + 0,  8, "Int")
  rectBottom  := NumGet(lprcMonitor + 0, 12, "Int")
  
  ;; Synchronizing indices.
  key := rectLeft . "-" . rectTop . "-" . rectRight . "-" . rectBottom
  If (Monitor.indices.HasKey(key)) {
    i := Monitor.indices[key]
    Logging_debug("Adding handle ``" . handle . "`` and DPI/ scaling information to monitor with key ``" . key . "``.", "Monitor_enumDisplayMonitorsProc")
  } Else {
    ;; Appending additional monitors not previously found.
    Logging_debug("Additional monitor with key ``" . key . "`` found.", "Monitor_enumDisplayMonitorsProc")
    i := Monitor.cache.Length() + 1
    Monitor_insert(i, rectLeft, rectTop, rectRight, rectBottom)
  }
  ;; Supplementing information (handle and scaling) to screen objects.
  Monitor.cache[i].handle := handle
  Monitor_getDpi(i)
  
	Return, 1
}

Monitor_getDpi(index) {
  Global Monitor

  If (Monitor.cache[index].handle != 0) {
    x := y := 0
    MDT_DEFAULT := MDT_EFFECTIVE_DPI := 0
    DllCall("SHcore\GetDpiForMonitor", "Ptr", Monitor.cache[index].handle, "Int", MDT_DEFAULT, "UInt*", x, "UInt*", y)

    dpiX := x
    dpiY := y
    scaleX := x / 96
    scaleY := y / 96
    Monitor.cache[index].scale := x / 96
  }
}
;; InnI: Get per-monitor DPI scaling factor (https://www.autoitscript.com/forum/topic/189341-get-per-monitor-dpi-scaling-factor/?tab=comments#comment-1359832)

Monitor_getObject(index, rectLeft, rectTop, rectRight, rectBottom) {
  Return, {index: index
         , ahkIndex: 0
         , handle: ""
         , key: rectLeft . "-" . rectTop . "-" . rectRight . "-" . rectBottom
         , name: ""

         , isPrimary: False
         , workArea: {}
         , x: rectLeft
         , y: rectTop
         , w: rectRight - rectLeft
         , h: rectBottom - rectTop
  
         , scale: 0
         , shellTrayWnd: ""}
}

Monitor_getShellTrayWnd(rectLeft, rectTop, rectRight, rectBottom) {
  shellTrayWndId := ""
  If (rectRight > rectLeft && rectBottom > rectTop) {
    For i, item in ["Shell_TrayWnd", "Shell_SecondaryTrayWnd"] {
      WinGet, wndId, List, % "ahk_class " . item
      Loop, % wndId {
        WinGetPos, wndX, wndY, wndW, wndH, % "ahk_id " . wndId%A_Index%
        x := Round(wndX + wndW / 2)
        y := Round(wndY + wndH / 2)
        If (x >= rectLeft && x <= rectRight && y >= rectTop && y <= rectBottom) {
          shellTrayWndId := wndId%A_Index%
          Logging_debug("Found *" . item . "* with id ``" . shellTrayWndId . "`` and center ``(" . x . ", " . y . ")`` "
                      . "inside ``(" . rectLeft . ", " . rectTop . ", " . rectRight . ", " . rectBottom . ")``.", "Monitor_getShellTrayWnd")
          Break
        }
      }
    }
  }
  Return, shellTrayWndId
}

Monitor_insert(index, rectLeft, rectTop, rectRight, rectBottom) {
  Global Monitor

  key := rectLeft . "-" . rectTop . "-" . rectRight . "-" . rectBottom
  Monitor.indices[key] := index
  Monitor.cache[index] := Monitor_getObject(index, rectLeft, rectTop, rectRight, rectBottom)
}

Monitor_matchWorkArea(rect, test := "center", variation := 5) {
  ;; `rect` must have keys x, y, w and h.
  ;; `test` may be "center" (center of rectangle on monitor?), "corners" (any corner inside monitor?) or "edge" (rectangle similar to monitor positions and dimensions?).
  ;; `variation` may be set to a number of pixels.
  Global Monitor

  m := 0
  If (rect.HasKey("x") && rect.HasKey("y") && rect.HasKey("w") && rect.HasKey("h")) {
    If (test == "center") {
      rect.x := rect.x + rect.w / 2
      rect.y := rect.y + rect.h / 2
      rect.w := rect.h := 0
/*    } Else If (test == "corners") {
      rect.x += variation, rect.w -= 2 * variation
      rect.y += variation, rect.h -= 2 * variation
*/
    }
    result := False
    For i, item in Monitor.cache {
      x := item.workArea.x, w := item.workArea.w
      y := item.workArea.y, h := item.workArea.h
      If (test == "center") {
        result := rect.x >= x && rect.y >= y && rect.x <= x + w && rect.y <= y + h
/*      } Else If (test == "corners") {
        result := (rect.x >= x && rect.y >= y && rect.x <= x + w && rect.y <= y + h)
               || (rect.x + rect.w >= x && rect.y >= y && rect.x + rect.w <= x + w && rect.y <= y + h)
               || (rect.x + rect.w >= x && rect.y + rect.h >= y && rect.x + rect.w <= x + w && rect.y + rect.h <= y + h)
               || (rect.x >= x && rect.y + rect.h >= y && rect.x <= x + w && rect.y + rect.h <= y + h)
      } Else If (test == "edge" && rect.w > 0 && rect.h > 0) {
        result := Abs(x - rect.x) < variation && Abs(y - rect.y) < variation && Abs(w - rect.w) < variation && Abs(h - rect.h) < variation
*/
      }
      If (result) {
        m := i
/*        Logging_debug("Monitor " . m . " matched " . (rect.w > 0 && rect.h > 0 ? "rectangle (" . test . ")" : "position")
                    . " (x = " . rect.x . ", y = " . rect.y . ", w = " . rect.w . ", h = " . rect.h . ").", "Monitor_matchWorkArea")
*/
        Break
      }
    }
  }
  Return, m
}

Monitor_writeCacheToFile(overwrite := False) {
  Global Monitor

  If (Monitor.filename != "") {
    text := "| idx | AHK | Handle     |         Monitor Key | Primary? | Taskbar  | Scale | WA-  X |      Y |  Width | Height | M-   X |      Y |  Width | Height | Display Name   |`n"
    text .= "| ---:| ---:|:---------- | -------------------:|:-------- |:-------- |:----- | ------:| ------:| ------:| ------:| ------:| ------:| ------:| ------:|:-------------- |`n"
    For i, item in Monitor.cache {
      ;; index, ahkIndex, handle, key, isPrimary, shellTrayWnd, scale, workArea.{x, y, w, h}, x, y, w, h, name
      text .= "| "
            . Format("{:3}",   item.index) . " | "
            . Format("{:3}",   item.ahkIndex) . " | "
            . Format("{:-10}", item.handle) . " | "
            . Format("{:19}",  item.key) . " | "
            . Format("{:-8}",  item.isPrimary ? "Yes" : "No") . " | "
            . Format("{:-8}",  item.shellTrayWnd) . " | "
            . Format("{:-5}",  SubStr(item.scale, 1, 5)) . " | "
            . Format("{:6}",   item.workArea.x) . " | "
            . Format("{:6}",   item.workArea.y) . " | "
            . Format("{:6}",   item.workArea.w) . " | "
            . Format("{:6}",   item.workArea.h) . " | "
            . Format("{:6}",   item.x) . " | "
            . Format("{:6}",   item.y) . " | "
            . Format("{:6}",   item.w) . " | "
            . Format("{:6}",   item.h) . " | "
            . Format("{:-14}", item.name) . " |`n"
    }
    If (overwrite) {
      FileDelete, % Monitor.filename
    }
    FileAppend, % text, % Monitor.filename
  }
}
