/*
:title:     bug.n - tiling window management
:copyright: (c) 2022 joten <https://codeberg.org/joten>
:license:  GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.txt)

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

/*
 :lib: VirtualDesktopAccessor (https://github.com/Ciantic/VirtualDesktopAccessor)
 :copyright: (c) Ciantic <https://github.com/Ciantic>
 :version: 5bc1bba updated on 2 Jun 2019
 :license: MIT license
 :requirements: VS 2017 runtimes vc_redist.x64.exe and/or vc_redist.x86.exe
*/

Desktop__init() {
  Global Desktop

  Desktop := {aDesktop: 0
            , hideout: 0
            , hVirtualDesktopAccessor: DllCall("LoadLibrary", "Str", A_ScriptDir . "\lib\virtual-desktop-accessor.dll", "Ptr")}

  Desktop.aDesktop := Desktop_getCurrentDesktopIndex()
  If (Desktop_getDesktopCount() > 1) {
    Desktop.hideout := (Desktop.aDesktop == 1) ? 2 : 1
    ;; Restart the virtual desktop accessor, when Explorer.exe crashes or restarts (e.g. when coming from a fullscreen game).
    OnMessage(DllCall("user32\RegisterWindowMessage", "Str", "TaskbarCreated"), "Desktop_restartVirtualDesktopAccessor")
    Logging_info("Window message **TaskbarCreated** registered.", "Desktop__init")
  } Else {
    Logging_warning("There are too few virtual desktops to utilize for hiding windows. Resolving to ``WinHide``.", "Desktop__init")
  }
}

Desktop_getCurrentDesktopIndex() {
  Global Desktop

  proc := DllCall("GetProcAddress", "Ptr", Desktop.hVirtualDesktopAccessor, "AStr", "GetCurrentDesktopNumber", "Ptr")
  ;; The desktop indices returned from virtual desktop accessor start at 0.
  ;; According to the AutoHotkey conventions these are shifted therewith starting at 1.
  Return, DllCall(proc, "UInt") + 1
}

Desktop_getDesktopCount() {
  Global Desktop

  proc := DllCall("GetProcAddress", "Ptr", Desktop.hVirtualDesktopAccessor, "AStr", "GetDesktopCount", "Ptr")
  Return, DllCall(proc, "UInt")
}

Desktop_getWindowDesktopIndex(winId) {
  Global Desktop

  proc := DllCall("GetProcAddress", "Ptr", Desktop.hVirtualDesktopAccessor, "AStr", "GetWindowDesktopNumber", "Ptr")
  Return, DllCall(proc, "UInt", winId) + 1
}

Desktop_moveWindowToDesktop(winId, index) {
  Global Desktop

  proc := DllCall("GetProcAddress", "Ptr", Desktop.hVirtualDesktopAccessor, "AStr", "MoveWindowToDesktopNumber", "Ptr")
  DllCall(proc, "UInt", winId, "UInt", index - 1)
}

Desktop_restartVirtualDesktopAccessor() {
  Global Desktop

  result := ""
  proc := DllCall("GetProcAddress", "Ptr", Desktop.hVirtualDesktopAccessor, "AStr", "RestartVirtualDesktopAccessor", "Ptr")
  DllCall(proc, "UInt", result)
  Return, result
}
